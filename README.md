# 오텍 오티스 script file

### 프로그램 선택

- /home/pi/projdect_name.sh : front와 inside 중에서 선택
    
    DEL_NAME=front : 기존 프로젝트 이름으로 삭제될 이름
    
    PRJ_NAME=insde : 실행할 프로젝트 이름
    

### 부팅시 프로그램 자동 실행

- etc/profile :  root권한이 아닌 pi 권한으로 실행되는 script
    
    cd /home/pi
    
    ./exec.sh
    
- home/pi/exec.sh : PRJ_NAME에 맞는 파이썬 파일 실행
    
    python $PRJ_NAMEl.py
    

### SSID 이름, ftp 접속 폴더를 PRJ_NAME에 맞게 변경

- /etc/init.d/user.sh —> etc/rc5.d/S99user
    - PRJ_NAME에 맞는 SSID 이름 변경
        
        /etc/hostapd/hostapd.conf에 ssid=parking-front가 있다면 삭제하고 ssid=parking-inside 항목을 write.
        
    - PRJ_NAME에 맞는 ftp 접속 폴더 변경
        
        /etc/vsftpd.conf에 local_root=/home/pi/front/src 가 있다면 삭제하고 local_root=/home/pi/inside/src 를 write