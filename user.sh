#!/bin/bash
#*sudo ln -s /etc/init.d/user.sh /etc/rc5.d/S99user
#*/etc/init.d/user.sh
#*/etc/init.d/user.sh <== S99user(symbolic link)
#*to change ssid=XXXX in /etc/hostapd/hostapd.conf
#*to change rocal_loot=XXXX in /etc/vsftpd.conf

project_file="/home/pi/project_name.sh"

#*Check if project.sh exists
if [ -f $project_file ]; then
    # Source (or .) the project.sh script to load its variables
    source $project_file

    # Check if PRJ_NAME is defined in project.sh
    if [ -n "DEL_NAME" ]; then
        echo "The project name is: $DEL_NAME"
    else
        echo "DEL_NAME is not defined in project.sh"
    fi

    if [ -n "PRJ_NAME" ]; then
        echo "The project name is: $PRJ_NAME"
    else
        echo "PRJ_NAME is not defined in project.sh"
    fi
else
    echo "project_name.sh not found."
fi


#*Define the filename and the string to search for
dest_file="/etc/hostapd/hostapd.conf"

search_string="ssid=parking-$DEL_NAME"
line_num=$(grep -n "$search_string" "$dest_file" | cut -d ':' -f 1)
if [ -n "$line_num" ]; then
    sed -i "${line_num}d" "$dest_file"
    echo "ssidprint: Delete $search_string in $dest_file."
else
    echo "ssidprint: Not found $search_string."
fi

search_string="ssid=parking-$PRJ_NAME"
line_num=$(grep -n "$search_string" "$dest_file" | cut -d ':' -f 1)
if [ -n "$line_num" ]; then
    echo "ssidprint: $search_string already exists in $dest_file."
else
    # If it doesn't exist, append the search string to the end of the file
    echo "$search_string" >> "$dest_file"
    echo "ssidprint: Added $search_string to $dest_file."
fi


#*Define the filename and the string to search for
dest_file="/etc/vsftpd.conf"

search_string="local_root=/home/pi/${DEL_NAME}/src"
line_num=$(grep -n "$search_string" "$dest_file" | cut -d ':' -f 1)
if [ -n "$line_num" ]; then
    sed -i "${line_num}d" "$dest_file"
    echo "ftpprint: Delete $search_string in $dest_file."
else
    echo "ftpprint: Not found $search_string."
fi

search_string="local_root=/home/pi/${PRJ_NAME}/src"
line_num=$(grep -n "$search_string" "$dest_file" | cut -d ':' -f 1)
if [ -n "$line_num" ]; then
    echo "ftpprint: $search_string already exists in $dest_file."
else
    # If it doesn't exist, append the search string to the end of the file
    echo "$search_string" >> "$dest_file"
    echo "ftpprint: Added $search_string to $dest_file."
fi
