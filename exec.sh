#!/bin/bash
#/home/pi/exec.sh
echo "==========================="
echo "$USER excutes $0"

project_file="/home/pi/project_name.sh"

# Check if project.sh exists
if [ -f $project_file ]; then
    # Source (or .) the project.sh script to load its variables
    source $project_file

    # Check if PRJ_NAME is defined in project.sh
    if [ -n "PRJ_NAME" ]; then
        echo "The project name is: $PRJ_NAME"
    else
        echo "PRJ_NAME is not defined in project_name.sh"
    fi
else
    echo "project_name.sh not found."
fi


##############################################################################
PRJ_FILE="$PRJ_NAME".py
PRJ_PATH=/home/pi/"$PRJ_NAME"/src/

sleep_time=5
max_retry=10
retry=0

shcount="$(ps -ef | grep $0 | grep -v grep | wc -l)"

echo "$0 count $shcount (count will be 2,3,4...)"

if [ $shcount -eq 2 ]
then

    while : # LOOP
    do

        pycount="$(ps -ef | grep $PRJ_FILE | grep -v grep | wc -l)"

        if [ $pycount -eq 0 ]
        then
            cd $PRJ_PATH
            pwd
            python $PRJ_FILE &
            ((retry++))
            echo python $PRJ_FILE
            echo "$PRJ_FILE count $pycount (count will be 1,2,3...)"
        else
            # echo "Already $PRJ_FILE is running, count $pycount. retry $retry max $max_retry."
	    retry=0
        fi

        if [ $retry -gt $max_retry ]
        then
	    echo "retry $retry"
	    echo "*******************"
	    echo "*** sudo reboot ***"
	    echo "*******************"
	    #sleep $sleep_time
	    #exit 0
            sudo reboot
        fi

        sleep $sleep_time
    done

else

   echo "Already $0 is running, count $shcount."
   echo "$0 quit"
   echo "--------------------------------------"

fi
